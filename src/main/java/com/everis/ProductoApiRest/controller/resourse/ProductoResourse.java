package com.everis.ProductoApiRest.controller.resourse;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductoResourse {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column
	private String nombre;
	@Column(unique = true)
	private String codigo;
	@Column
	private String descripcion;
	@Column
	private BigDecimal precio;
	@ManyToOne
	private TipoProductoResourse tipoProducto;
	@Column
	private Boolean activo = true; 
}
