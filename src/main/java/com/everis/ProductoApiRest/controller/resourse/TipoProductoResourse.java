package com.everis.ProductoApiRest.controller.resourse;

import lombok.Data;

@Data
public class TipoProductoResourse {

	private String nombre;
	private String codigo;

}
