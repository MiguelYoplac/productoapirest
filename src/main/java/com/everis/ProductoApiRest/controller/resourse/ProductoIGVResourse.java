package com.everis.ProductoApiRest.controller.resourse;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ProductoIGVResourse {

	private Long id;
	private String nombre;
	private String codigo;
	private BigDecimal precio; 
	private BigDecimal precioNeto; //(Calcular precio + precio*(igv)) 
	
}
