package com.everis.ProductoApiRest.controller.resourse;

import java.math.BigDecimal;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductoReducidoResourse {

	private String nombre;
	private String codigo;
	private String descripcion;
	private BigDecimal precio;
	private String codigoTipoProducto; 
}
