package com.everis.ProductoApiRest.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.ProductoApiRest.controller.resourse.ProductoIGVResourse;
import com.everis.ProductoApiRest.controller.resourse.ProductoReducidoResourse;
import com.everis.ProductoApiRest.controller.resourse.ProductoResourse;
import com.everis.ProductoApiRest.controller.resourse.TipoProductoResourse;
import com.everis.ProductoApiRest.model.entity.Producto;
import com.everis.ProductoApiRest.model.entity.TipoProducto;
import com.everis.ProductoApiRest.service.ProductoService;

@RestController
public class ApiController {
	
	@Value("${igv}")
	BigDecimal igv;
	
	@GetMapping("/igv")
	public BigDecimal igv() {
		return igv;
	}
	
	@Autowired
	@Qualifier("productoServiceImpl")
	ProductoService productoService;
	
	@GetMapping("/productos")
	public List<ProductoResourse> obtenerProductos() {
		
		List<ProductoResourse> listado = new ArrayList<>();
		productoService.obtenerProductos().forEach(producto -> {
			ProductoResourse productoResourse = new ProductoResourse();
			productoResourse.setId(producto.getId());
			productoResourse.setNombre(producto.getNombre());
			productoResourse.setCodigo(producto.getCodigo());
			productoResourse.setDescripcion(producto.getDescripcion());
			TipoProductoResourse tipoProducto = new TipoProductoResourse();
			tipoProducto.setNombre(producto.getTipoProducto().getNombre());
			tipoProducto.setCodigo(producto.getTipoProducto().getCodigo());
			productoResourse.setTipoProducto(tipoProducto);
			productoResourse.setPrecio(producto.getPrecio());
			if(producto.getActivo()==true) {
				listado.add(productoResourse);
			}			
		});
		return listado;
	}
	
	@GetMapping("/productos/{id}")
	public ProductoIGVResourse obtenerProductoPorId(@PathVariable("id") long id) throws Exception {
		Producto producto = productoService.obtenerProductoPorId(id);
		ProductoIGVResourse productoIGVResourse = new ProductoIGVResourse();
		productoIGVResourse.setId(producto.getId());
		productoIGVResourse.setNombre(producto.getNombre());
		productoIGVResourse.setCodigo(producto.getCodigo());
		productoIGVResourse.setPrecio(producto.getPrecio());
		productoIGVResourse.setPrecioNeto(producto.getPrecio().add(producto.getPrecio().multiply(igv)));
		return productoIGVResourse;
	}
	
	@PostMapping("/productos")
	public ResponseEntity<ProductoResourse> guardarProducto(@RequestBody ProductoReducidoResourse request) throws Exception {	
		Producto producto = new Producto();
		producto.setNombre(request.getNombre());
		producto.setCodigo(request.getCodigo());
		producto.setDescripcion(request.getDescripcion());
		producto.setPrecio(request.getPrecio());		
		TipoProducto tipoProducto = productoService.obtenerTipoProductoPorCodigo(request.getCodigoTipoProducto());
		producto.setTipoProducto(tipoProducto);
//		tipoProducto.setCodigo(request.getCodigoTipoProducto());		
//		producto.setTipoProducto(tipoProducto);
		producto.setActivo(true);
		Producto productoNuevo = productoService.insertar(producto);
		ProductoResourse productoResourse = new ProductoResourse();
		TipoProductoResourse tipoProductoResourse = new TipoProductoResourse();
		tipoProductoResourse.setNombre(productoNuevo.getTipoProducto().getNombre());
		tipoProductoResourse.setCodigo(productoNuevo.getTipoProducto().getCodigo());
		productoResourse.setId(productoNuevo.getId());
		productoResourse.setNombre(productoNuevo.getNombre());
		productoResourse.setCodigo(productoNuevo.getCodigo());
		productoResourse.setDescripcion(productoNuevo.getDescripcion());
		productoResourse.setPrecio(productoNuevo.getPrecio());
		productoResourse.setTipoProducto(tipoProductoResourse);
		return new ResponseEntity<>(productoResourse, HttpStatus.CREATED);
	}
	
	@PutMapping("/productos/{codigo}")
	public ResponseEntity<ProductoResourse> modificarProducto(@PathVariable("codigo") String codigo, 
			@RequestBody ProductoResourse producto) throws Exception {
		
		Producto update = new Producto();
		update.setNombre(producto.getNombre());
		update.setCodigo(codigo);
		
		Producto pro = productoService.modificarProducto(update);
		ProductoResourse productoResource = new ProductoResourse();
		productoResource.setId(pro.getId());
		productoResource.setNombre(pro.getNombre());

	   return new ResponseEntity<>(productoResource, HttpStatus.OK);
	}
	
	@PutMapping("/productos/borrar/{codigo}")
	public ResponseEntity<ProductoResourse> borrarProducto(@PathVariable("codigo") String codigo) throws Exception {
		
		Producto update = new Producto();
		update.setActivo(false);
		update.setCodigo(codigo);
		
		Producto pro = productoService.borrarProducto(update);
		ProductoResourse productoResource = new ProductoResourse();
		productoResource.setActivo(pro.getActivo());

	   return new ResponseEntity<>(productoResource, HttpStatus.OK);
	}
	
	@GetMapping(value="/productos/eliminar/{codigo}")
	public List<ProductoResourse> eliminarProducto(@PathVariable(value="codigo") String codigoProducto) throws Exception {
		Producto productoEliminar = productoService.obtenerProductoPorCodigo(codigoProducto);
		productoService.eliminarProducto(productoEliminar);

		List<ProductoResourse> listado = new ArrayList<>();
		productoService.obtenerProductos().forEach(producto -> {
			ProductoResourse productoResourse = new ProductoResourse();
			productoResourse.setId(producto.getId());
			productoResourse.setNombre(producto.getNombre());
			productoResourse.setCodigo(producto.getCodigo());
			productoResourse.setDescripcion(producto.getDescripcion());
			TipoProductoResourse tipoProducto = new TipoProductoResourse();
			tipoProducto.setNombre(producto.getTipoProducto().getNombre());
			tipoProducto.setCodigo(producto.getTipoProducto().getCodigo());
			productoResourse.setTipoProducto(tipoProducto);
			productoResourse.setPrecio(producto.getPrecio());
			listado.add(productoResourse);
		});
		return listado;
	}
	
//	@GetMapping(value="/productos/eliminar/{codigo}")
//	public List<ProductoResourse> borrarProducto(@PathVariable(value="codigo") String codigoProducto) throws Exception {
//		Producto productoEliminar = productoService.obtenerProductoPorCodigo(codigoProducto);
//		productoService.eliminarProducto(productoEliminar);
//
//		List<ProductoResourse> listado = new ArrayList<>();
//		productoService.obtenerProductos().forEach(producto -> {
//			ProductoResourse productoResourse = new ProductoResourse();
//			productoResourse.setId(producto.getId());
//			productoResourse.setNombre(producto.getNombre());
//			productoResourse.setCodigo(producto.getCodigo());
//			productoResourse.setDescripcion(producto.getDescripcion());
//			TipoProductoResourse tipoProducto = new TipoProductoResourse();
//			tipoProducto.setNombre(producto.getTipoProducto().getNombre());
//			tipoProducto.setCodigo(producto.getTipoProducto().getCodigo());
//			productoResourse.setTipoProducto(tipoProducto);
//			productoResourse.setPrecio(producto.getPrecio());
//			listado.add(productoResourse);
//		});
//		return listado;
//	}
}
