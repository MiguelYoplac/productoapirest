package com.everis.ProductoApiRest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.everis.ProductoApiRest.model.entity.Producto;
import com.everis.ProductoApiRest.model.entity.TipoProducto;
import com.everis.ProductoApiRest.model.repository.ProductoRepository;
import com.everis.ProductoApiRest.model.repository.TipoProductoRepository;

@Service
public class ProductoServiceImpl implements ProductoService {
	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Override
	public Iterable<Producto> obtenerProductos(){
		return productoRepository.findAll();
	}

	@Override
	public Producto insertar(Producto producto) throws Exception {
//		TipoProducto tipoProducto = new TipoProducto(); 
//		tipoProducto = tipoProductoRepository.findByCodigo(producto.getTipoProducto().getCodigo()).orElseThrow(()->new Exception("Tipo de Producto no Encontrado"));
//		producto.setTipoProducto(tipoProducto);
		return productoRepository.save(producto);
	}
	
	@Override
	public Producto obtenerProductoPorId(long id) throws Exception {
		return productoRepository.findById(id).orElseThrow(()->new Exception("Producto no Encontrado"));
	}
	
	@Autowired
	private TipoProductoRepository tipoProductoRepository;
	
	@Override
	public TipoProducto obtenerTipoProductoPorCodigo(String codigo) throws Exception{
		return tipoProductoRepository.findByCodigo(codigo).orElseThrow(()->new Exception("Tipo de Producto no Encontrado"));
	}
	
	@Override
	public Producto obtenerProductoPorCodigo(String codigo) throws Exception {
		return productoRepository.findByCodigo(codigo).orElseThrow(()->new Exception("Producto no Encontrado"));
	}
	
	@Override
	public Producto modificarProducto(Producto producto) throws Exception{
		return productoRepository.findByCodigo(producto.getCodigo())
		        .map(productoUpdate -> {
		        	productoUpdate.setNombre(producto.getNombre());
		        	productoRepository.save(productoUpdate);
		            return productoUpdate;
		    })
		        .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Producto no encontrado"));
	}
	
	@Override
	public Producto borrarProducto(Producto producto) throws Exception{
		return productoRepository.findByCodigo(producto.getCodigo())
		        .map(productoUpdate -> {
		        	productoUpdate.setActivo(producto.getActivo());
		        	productoRepository.save(productoUpdate);
		            return productoUpdate;
		    })
		        .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Producto no encontrado"));
	}
	
	@Override
	public void eliminarProducto(Producto producto) {
		productoRepository.delete(producto);	
	}

}
