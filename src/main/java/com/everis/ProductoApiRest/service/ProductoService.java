package com.everis.ProductoApiRest.service;

import com.everis.ProductoApiRest.model.entity.Producto;
import com.everis.ProductoApiRest.model.entity.TipoProducto;

public interface ProductoService {
	
	//LISTAR
	
	public Iterable<Producto> obtenerProductos();
	
	//GUARDAR
	
	public Producto insertar(Producto producto) throws Exception;
	
	//OBTENER
	
	public Producto obtenerProductoPorId(long id) throws Exception;
	
	public Producto obtenerProductoPorCodigo(String codigo) throws Exception;
	
	public TipoProducto obtenerTipoProductoPorCodigo(String codigo) throws Exception;
	
	//MODIFICAR
	
	public Producto modificarProducto(Producto producto) throws Exception;
	
	//ELIMINAR
	
	public Producto borrarProducto(Producto producto) throws Exception;
	
	public void eliminarProducto(Producto producto);
}
