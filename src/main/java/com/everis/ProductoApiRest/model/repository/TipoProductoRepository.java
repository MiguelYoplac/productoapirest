package com.everis.ProductoApiRest.model.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.ProductoApiRest.model.entity.TipoProducto;

@Repository
public interface TipoProductoRepository extends CrudRepository<TipoProducto, Long> {

	public Optional<TipoProducto> findByCodigo(String codigo);
}
