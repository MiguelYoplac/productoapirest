package com.everis.ProductoApiRest.model.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.ProductoApiRest.model.entity.Producto;

@Repository
public interface ProductoRepository extends CrudRepository<Producto, Long> {

	public Optional<Producto> findByCodigo(String codigo);

}
